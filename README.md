## Virtual Slot Machine
A little test game that simulates a virtual slot machine.
It is written in Python 2.7 and QT.

Required Libraries are QtPy and either PySide, pyQt4 or pyQt5.
It has been tested on Win7 and OSX.
Minimum Screen Resolution is 1152 x 864

To start simply run:
msm_application.py


