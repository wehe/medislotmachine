"""
Marco Wehe, 18th February 2018
"""
import sys

from qtpy.QtCore import *  # @UnusedWildImport
from qtpy.QtGui import *  # @UnusedWildImport
from qtpy.QtWidgets import *  # @UnusedWildImport
from qtpy import uic

import time
#from ui_slot_machine import Ui_SlotMachine
from msm_engine import SlotMachineEngine

class Ui(QMainWindow):
	def __init__(self):
		super(Ui, self).__init__()
		uic.loadUi('msm_main_window.ui', self)
		self.initUi()
		self.show()
		
	def initUi(self):
		# build ui
		self.setWindowTitle('Medi Super Slot Machine')
		self.setFixedSize(self.size()) #Layout has not been designed for full screen mode or different sizes!
		
		# Original Dev Buttons - Keeping them hidden in case the graphics
		# don't work on some systems.
		self.button_play.clicked.connect(self.start_play)
		self.button_play.hide()
		self.button_stop.clicked.connect(self.stop_play)
		self.button_stop.hide()
		
		# Credit Button
		self.button_credits.clicked.connect(self.insert_credits)
		
		# Starting the Engine
		self.engine = SlotMachineEngine(ui=self)
		
		self.button_play.setStyleSheet("background-color: #cbb976")
		self.button_credits.setStyleSheet("background-color: #cbb976")
		self.button_stop.setStyleSheet("background-color: #a7322a")
		
		# Make Image Button Spin / Brain
		self.brain_map_normal = QPixmap("bin/brain_button_normal.png")
		self.brain_map_active = QPixmap("bin/brain_button_active.png")
		self.label_play.setPixmap(self.brain_map_normal)
		self.label_play.setGeometry(QRect(440, 520, 200, 220))
		self.label_play.installEventFilter(self)
		
		# Make Image Button Stop / Heart
		self.heart_map_normal = QPixmap("bin/heart_button_normal.png")
		self.heart_map_active = QPixmap("bin/heart_button_active.png")
		self.label_stop.setPixmap(self.heart_map_normal)
		self.label_stop.setGeometry(QRect(230, 520, 170, 220))
		self.label_stop.installEventFilter(self)
		
		# Load Slot Symbols
		self.maps = {}
		for symbol in self.engine.symbol_to_tex_matrix:
			self.maps[symbol] = QPixmap("bin/"+self.engine.symbol_to_tex_matrix[symbol] )
		# Set Initial Reels
		self.set_reels()
		
		# Load Overlay
		self.map_overlay = QPixmap("bin/overlay.png")
		self.label_overlay.setPixmap(self.map_overlay)
		self.label_overlay.show()
		# Lift Overlay
		self.frame_overlay.raise_()
		
		# Set LCD Colour
		light_blue = QPalette(QColor(231,244,235))
		self.lcd_number_jackpot.setPalette(light_blue)
		self.lcd_number_credit.setPalette(light_blue)
		self.lcd_number_wallet.setPalette(light_blue)
		self.lcd_number_lastwin.setPalette(light_blue)
		
		# Update LCD Number
		self.update_lcd()
				
	def eventFilter(self, source, event):
		if event.type() == QEvent.MouseButtonPress:
			if source.objectName() == "label_play":
				self.label_play.setPixmap(self.brain_map_active)
			elif source.objectName() == "label_stop":
				self.label_stop.setPixmap(self.heart_map_active)
		
		elif event.type() == QEvent.MouseButtonRelease:
			if source.objectName() == "label_play":
				self.label_play.setPixmap(self.brain_map_normal)
				self.start_play()
			elif source.objectName() == "label_stop":
				self.label_stop.setPixmap(self.heart_map_normal)
				self.stop_play()
					
		return super(Ui, self).eventFilter(source, event)
	
	def paintEvent(self, event):
		"""
		We don't use the tiles but it's nice to have in case we want to change the layout
		"""
		self.tile= QPixmap("bin/background.png")
		painter = QPainter(self)
		painter.drawTiledPixmap(self.rect(), self.tile)
		super(Ui, self).paintEvent(event)
	
	def closeEvent(self, event):
		"""
		Without this call python might not exit properly on some systems,
		even if the GUI is closed. 
		"""
		self.engine.message('Closing...')
		if self.engine.spinning_active:
			# Shutting down the active spin and giving the
			# loop time to shut down. Would be nicer with a thread.
			self.engine.spinning_active = False
			for _ in range(0,10):
				app.processEvents()
				time.sleep(0.01)
		return None
		
	def start_play(self):
		if not self.engine.spinning_active:
			if self.engine.remove_credit():
				self.engine.message('Starting new Play...')
				self.button_play.setEnabled(False)
				self.label_play.setEnabled(False)
				self.engine.add_jackpot()
				self.update_lcd()
				self.engine.result_generator()
				self.engine.set_reel_spin_offsets()
				# Starting to Spin the Reels
				self.engine.spinning_active = True
				self.engine.message("Spinning... Good Luck!")
				ramp = 8
				while self.engine.spinning_active == True:
					
					self.engine.animate_reel_spin_offsets()
					self.set_reels()
					# Pause and Process Loop
					for _ in range(0,20):
						app.processEvents()
						time.sleep(0.0025+(ramp*0.00025))
						
					if ramp > 1: ramp -= 1
				
				self.engine.process_game_outcome()
				self.update_lcd()
				self.button_play.setEnabled(True)
				self.label_play.setEnabled(True)
				self.engine.stopping_active = False
				return True
					
			else:
				self.engine.message('Not enough money. Insert Credits.')
	
	def stop_play(self):
		if self.engine.spinning_active and not self.engine.stopping_active:
			self.engine.stopping_active = True
			self.engine.message('Stop Spin Early...')
			self.engine.stop_reel_spin()
		
	def insert_credits(self):
		self.engine.message('Insert Money...')
		self.engine.add_credit(self.engine.game_cost)
		self.update_lcd()
	
	def update_lcd(self):
		self.lcd_number_credit.display(str(self.engine.credit))
		self.lcd_number_credit.update()

		self.lcd_number_jackpot.display(str(self.engine.jackpot))
		self.lcd_number_jackpot.update()
		
		self.lcd_number_lastwin.display(str(self.engine.lastwin))
		self.lcd_number_lastwin.update()
		
		self.lcd_number_wallet.display(str(self.engine.wallet))
		self.lcd_number_wallet.update()


	def set_reels(self):
		# Get the display table that tells us which
		display_table = self.engine.symbol_display_table()
		for xreel in display_table:
			for xrow in display_table[xreel]:
				label = getattr(self,'label_result_reel'+str(xreel)+"_row"+str(xrow))
				symbol = display_table[xreel][xrow]
				if symbol == "_":
					label.hide()
				else:
					label.setPixmap(self.maps[symbol])					
					label.show()
					label.update()
		return True

if __name__ == '__main__':
	app = QApplication(sys.argv)
	window = Ui()
	sys.exit(app.exec_())