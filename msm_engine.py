from random import randrange
import itertools, re, collections
		
		
class SlotMachineEngine():
	def __init__(self, ui=None, logic="fair", reels=4):
		"""
		Arguments:
		logic: Options are "fair" or "casino"
		symbols: Integer > 0
		 
		Description:
		This class holds all of the virtual slot machine properties.
		Such as credits and the last result. It also defines the layout of
		the reels and the stop matrix.

		Terminology
		tex = "name_of_image_A.png"
		symbol = Letter description of a tex, i.e. A, B
		stop = Virtual stop on the reel, represented by a number
		reel = Virtual slot machine reel
		"""
		self.ui = ui
		
		
		# This is the only argument test we do and it will simply
		# print an error. Since this is not a user input field no more testing has to be
		# done at this stage.
		if logic.lower() in ["fair", "casino"]:
			self.logic = logic.lower()
		else:
			print "ERROR: Logic Setting can only be Fair or Casino!"
			#TODO: At this stage a custom error could be added to increase safety

		# The number of reels as defined during init
		self.reels = reels
		# Symbol matrix matches an alphabetic result to a graphic or text
		self.symbol_to_tex_matrix = {
			"_":"blank",
			"A":"symbol_a.png",
			"B":"symbol_b.png",
			"C":"symbol_c.png",
			"D":"symbol_d.png",
			"E":"symbol_e.png",
			"F":"symbol_f.png" }

		# The total number of symbols our matrix contains
		self.symbol_total = len(self.symbol_to_tex_matrix)

		# The order of the symbols on the reels. We don't just want to line them all up but
		# create a longer "mixed" order.
		self.reel_symbol_order = ["A", "B", "C", "D", "E", "F", "A", "B", "C", "D", "F"]

		# The 11 symbols will actually translate into 22 stops with blanks before every symbol
		self.stops_total = len(self.reel_symbol_order)*2

		# Final Reel Matrix after Blanks were added. This Matrix contains all the stops and the displayed result
		# i.e. which symbol or gap position
		self.stops_to_symbol_matrix = self.stops_maker()

		# The latest spin result from the ACTIVE play
		self.spin_result_symbols = "" #Setting the initial result to a jackpot to motivate users
		self.spin_stops_generated = {} 
		# Setting initial defaults (Support for X-Amount of Reels)
		for xreel in range(1, self.reels+1):
			self.spin_result_symbols += "A" 
			self.spin_stops_generated[xreel] = 2
			
		# The previous game results
		self.spin_result_last = ""
		self.spin_stops_last = {}
		# Setting initial defaults (Support for X-Amount of Reels)
		for xreel in range(1, self.reels+1):
			self.spin_result_last += "A"
			self.spin_stops_last[xreel] = 2
		
		# Current Player Credits
		self.credit = 10
		self.game_cost = 1
		self.wallet = 0
		self.lastwin = 0
		#TODO: Load\Store Jackpot from\to file to remember it
		self.jackpot = 100 # Initial Jackpot
		self.jackpot_auto_topup = True # Should the jackpot get topped up?
		self.jackpot_low_threshold = 25 # At what level should the jackpot get topped up?
		self.jackpot_top_up = 100 # How much should be ADDED to the jackpot?
		
		# Number of spins
		self.spins = [2,3,4] # These are the possible fully reel spins for each game. This determines game duration.
		self.rotate_steps = 1 
		self.spinning_active = False
		self.stopping_active = False
		self.spin_reel_offsets = {}
		for xreel in range(1, self.reels+1): self.spin_reel_offsets[xreel] = 0
		
		self.message("The Medi Virtual Slot Machine is Ready! Insert some coins and start to spin!")
	
		
	def message(self, msg):
		if self.ui: self.ui.statusBar().showMessage(msg)
		else: pass # No UI to print to
		print (msg)
		
	def stops_maker(self):
		"""
		Simply compiles the final stops matrix dictionary.
		Adding blanks after each symbol. Returning:
		{1: 'blank', 2: 'A', 3: 'blank'... 
		Note: This is generated at init. Please use self.stop_matrix instead.
		"""		
		_full_reel = {}
		for xpos in range(1, self.stops_total+1, 2):
			# Add Blank
			_full_reel[xpos] = "_" #Blank
			# Add Symbol Key
			_full_reel[xpos+1] = self.reel_symbol_order[((xpos+1)/2)-1]
		return _full_reel
	
	def find_unique_sequencer(self):
		if "_" in self.spin_result_symbols:
			# Blanks can't win this round!
			return False
		else:
			max_letter_count = collections.Counter(self.spin_result_symbols).most_common(1)[0][1]
			if max_letter_count == 1:
				# All letters are unique!
				return True
						
	def find_repeats(self, repetition):
		"""
		Repetition: Int > 1
		"""
		# Find all repeats
		candidates = set(re.findall(r'([A-Z])\1', self.spin_result_symbols))
		# Declare search logic
		logic = r"("+str("{0}"*repetition)+"+)"
		# And find the repetitions blocks
		repeats = itertools.chain(*[re.findall(logic.format(c), self.spin_result_symbols) for c in candidates])
		# return results in case of later evaluation. I.e. higher scoring pairs
		return [x for x in repeats if len(x) == repetition]
	
	def result_generator(self):
		# Before generating a new code we transfer the current stop into the
		# last spin variable, which we will need for the animation later.
		for xreel in range(1, self.reels+1):
			self.spin_stops_last[xreel] = self.spin_stops_generated[xreel]
		self.spin_result_symbols_last = self.spin_result_symbols
		
		# Placing results into private temp variable
		# This will avoid risks of triggering anything else 
		# by updating the class result before the whole game is finished.
		_spin_results = []
		# Note: spin_stops stays live as they are used for animation only but
		# not the triggering of wins!
		
		if self.logic == "fair":
			"""
			Generates a fair result. This means each symbol on the reel and each blank have the same chance of appearing.
			Only exception is that some symbols are more frequently on the reel. Following the Double Diamond concept.
			"""
			for xreel in range(1,self.reels+1):
				_result = randrange(1, self.stops_total+1) 
				self.spin_stops_generated[xreel] = _result
				_symbol = self.stops_to_symbol_matrix[_result]
				_spin_results.append(_symbol)
				print "reel",xreel, ":", _result, self.symbol_to_tex_matrix[_symbol]
		
		elif self.logic == "casino":
			#TODO: Implement Casino Mode
			"""
			A true Casino Slot has multiple virtual stops to reduce chances of winning even
			more. This has not yet been added.
			""" 
			pass
		
		else:
			print "ERROR: No valid logic configured!"
		
		# Evaluating Win & Loss
		self.spin_result_symbols = "".join(_spin_results)
		print "Results:", self.spin_result_symbols
	
	def process_game_outcome(self):
		"""
		Searches are arranged in order of winning potential...
		Each win will trigger the associated winning function.
		"""
		if self.find_repeats(4): 
			# Jackpot
			self.win_jackpot_full()
		elif self.find_unique_sequencer(): 
			# Halfpot! DISTINCT!
			self.win_jackpot_half()
		elif self.find_repeats(3): 
			# Triple Win
			self.win_play(3)
		elif self.find_repeats(2): 
			# Double Win
			self.win_play(2)
		else:
			# Lost
			self.message( "You WON NOTHING, try again...!" )

	def symbol_display_table(self): 
		"""
		Returns dict[reel INT][ROW INT] = STOP Value
		"""
		all_reels_symbol_dict = {}
		
		for xreel in range(1, self.reels+1):
			
			reel_symbols_dict = {}

			# Get stop id for  row 3 (middle), add rotate and scale value into range
			mid_stop = self.stops_into_range( self.spin_reel_offsets[xreel] )
			flip={1:5,2:4,3:3,4:2,5:1}
			for xrow in [1,2,3,4,5]:
				offset_id = mid_stop + 3 - xrow + 22
				stop_id = self.stops_into_range(offset_id)
				symbol = self.stops_to_symbol_matrix[stop_id]
				reel_symbols_dict[flip[xrow]] = symbol
				
			all_reels_symbol_dict[xreel] = reel_symbols_dict
			
		return all_reels_symbol_dict
		return True
	
	def set_reel_spin_offsets(self):
		"""
		We are giving each reel a random offset from the generated result.
		Starting with the last shown icon and moving up from there onto a 
		higher virtual spin reel
		"""
		# Pick a Different Rotation per Reel
		for xreel in range(1, self.reels+1):
			# Pick a Possible Spin Amount
			possible_spin_choices= len(self.spins)
			if possible_spin_choices > 1:
				spin_choice = randrange(0,len(self.spins))
			else: spin_choice = 0
			
			spins = self.spins[spin_choice]
			ts = spins * self.stops_total
			new_spin_stops = ts + self.spin_stops_last[xreel]
			self.spin_reel_offsets[xreel] = new_spin_stops
		return True

	def animate_reel_spin_offsets(self):
		"""
		Here we are continuously counting down the stop each reel is
		currently on until it has hit it's generated result.
		"""
		rotating = False

		for xreel in range(1, self.reels+1):
			new_spin_stop = self.spin_reel_offsets[xreel] - self.rotate_steps
			
			if new_spin_stop <= self.spin_stops_generated[xreel]: 
				self.spin_reel_offsets[xreel] = self.spin_stops_generated[xreel]
			else:
				self.spin_reel_offsets[xreel] = new_spin_stop
				rotating = True
		
		if rotating: self.spinning_active = True
		else: self.spinning_active = False
		#print self.spin_reel_offsets
		return True
	
	def stop_reel_spin(self):
		"""
		This sets each current stop to the stop nearest to 0
		and so at the end of the spin. This will look to the user as
		if the choice is made earlier.
		"""
		for xreel in range(1, self.reels+1):
			self.spin_reel_offsets[xreel] = self.stops_into_range(self.spin_reel_offsets[xreel] )
	
	def stops_into_range(self, number):
		"""
		Simply loops the entered number to keep
		it on the reel and help us with continuous
		rotations.
		"""
		if number > 22:
			mult = int(number/22)
			number = number-(mult*22)
			if number == 0: return 22
			else: return number
		elif number == 0: return 22
		else:
			return number
		return True
	
	"""
	The Monies
	"""
	def add_credit(self, amount):
		self.credit += amount
	
	def remove_credit(self):
		if self.credit >= self.game_cost:
			self.credit -= self.game_cost
			return True
		else:
			return False
			
	def add_jackpot(self):
		self.jackpot += self.game_cost
		return True
	
	def win_jackpot_half(self):
		#TODO: Release Money Here
		self.update_wallet(self.jackpot / 2)
		self.jackpot = self.jackpot / 2
		if self.jackpot <= self.jackpot_low_threshold: self.jackpot += self.jackpot_top_up
		self.top_up_jackpot()
	
	def win_jackpot_full(self):
		#TODO: Release Money Here
		self.message("JACKPOT! You have won: "+str(self.jackpot)+" Virtual Coins!")
		self.update_wallet(self.jackpot)
		self.top_up_jackpot()
	
	def win_play(self, number):
		win = number * self.game_cost
		word = {1:"Single", 2:"Double", 3:"Triple"}
		if self.jackpot < win:
			self.add_credit(win)
			self.message(word[number]+'! You have won '+str(number)+' free games!')
		else:
			self.jackpot -= win
			self.update_wallet(win)
			self.message(word[number]+' Win! You have won '+str(number)+' Virtual Coins!')
		
		self.top_up_jackpot()
		
	def update_wallet(self, number):
		self.lastwin = number
		self.wallet += number

	
	def top_up_jackpot(self):
		if self.jackpot_auto_topup:
			print "Topping up Jackpot..."
			if self.jackpot <= self.jackpot_low_threshold: 
				self.jackpot += self.jackpot_top_up
